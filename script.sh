#!/bin/bash

curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh

sudo usermod -aG docker $USER

rm get-docker.sh

sudo curl -L https://github.com/docker/compose/releases/download/1.20.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose


git clone https://github.com/faulus/ministation.git

cd ministation/ 

sudo docker-compose up -d 

